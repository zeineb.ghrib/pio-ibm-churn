import os
import math
import previsionio as pio
from dotenv import load_dotenv, find_dotenv
from src.pio_training import UC_NAME

# a best practice found here:
# https://drivendata.github.io/cookiecutter-data-science/
# find .env automatically by walking up directories until it's found
dotenv_path = find_dotenv()

# load up the entries as environment variables
load_dotenv(dotenv_path)
URL = os.environ.get("URL")
TOKEN = os.environ.get("TOKEN")

pio.client.init_client(URL, TOKEN)

# We add all Plotly and Dash necessary librairies
import dash
import plotly.graph_objects as go
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

# set local dirs
HOME_PATH = os.getcwd()
DATA_PATH = os.path.join(HOME_PATH, 'data')
ASSETS_PATH = os.path.join(HOME_PATH, 'assets')

# connect to your instance
pio.client.init_client(URL, TOKEN)
# get the related use case from name
uc = pio.Classification.from_name(UC_NAME)
# GET THE BEST MODEL
best = uc.best_model

# GET MODEL FEATURE IMPORTANCE
TOP = 10
# feature importance is already stocked in feature_importance property of "best" pio model
df_feature_importances = best.feature_importance[:TOP]
# Create a Features Importance Bar Chart
fig_features_importance = go.Figure()
fig_features_importance.add_trace(go.Bar(x=df_feature_importances["feature"],
                                         y=df_feature_importances["importance"],
                                         marker={'color': "#E74C3C"})
                                  )
fig_features_importance.update_layout(title_text='<b>Features Importance of the model<b>', title_x=0.5)

# We create perfomances Bar Chart
# we will get the performances along with the threshold that optimize the f1_score
performances = best.get_dynamic_performances(threshold=best.optimal_threshold)
perfs = {'accuracy': performances.get('accuracy'),
         'precision': performances.get('precision'),
         'recall': performances.get('recall'),
         'f1_score': 2 * (performances.get('precision') * performances.get('recall')) / (
                     performances.get('precision') + performances.get('recall'))}

fig_perfs = go.Figure()
fig_perfs.add_trace(go.Bar(y=list(perfs.keys()),
                           x=list(perfs.values()),
                           marker={'color': "#E74C3C"},  # 'rgb(171, 226, 251)'),
                           orientation='h')
                    )
fig_perfs.update_layout(title_text='<b>Best Model Performances<b>', title_x=0.5)

cat_children, linear_children = [], []
for index, var in uc.schema.iterrows():
    if var['type'] == 'select':
        cat_children.append(html.H4(children=var['name']))
        sorted_modalities = var['options']
        # create a Dropdown for each categorical variable
        cat_children.append(dcc.Dropdown(
            id='{}-dropdown'.format(var['name']),
            options=[{'label': value, 'value': value} for value in sorted_modalities],
            value=sorted_modalities[0]
        ))
    else:
        # linear children
        linear_children.append(html.H4(children=var['name']))
        middle = (int(var['max']) + int(var['min'])) / 2
        step = int((int(var['max']) - int(var['min'])) / 10)
        # create a slider for each numeric variable
        linear_children.append(dcc.Slider(
            id='{}-dropdown'.format(var['name']),
            min=math.floor(var['min']),
            max=round(var['max']),
            step=None,
            value=round(middle),
            marks={i: '{}'.format(i) for i in
                   range(int(var['min']), int(var['max']) + 1, max(step, 1))}
        ))

app = dash.Dash(__name__,
                # external CSS stylesheets
                external_stylesheets=[
                    "https://rawcdn.githack.com/Athena75/IBM-Customer-Value-Dashboarding/df971ae38117d85c8512a72643ce6158cde7a4eb/assets/style.css"
                ]
                )

# We apply basic HTML formatting to the layout
app.layout = html.Div(children=[
    # first row : Title
    html.Div(children=[
        html.Div(children=[html.H1(children="Simulation Tool : IBM Customer Churn")],
                 className='title'),

    ],
        style={"display": "block"}),
    # second row :
    html.Div(children=[
        # first column : fig feature importance + linear + prediction
        html.Div(children=[
            html.Div(children=[dcc.Graph(figure=fig_features_importance, className='graph')] + linear_children),
            # prediction result
            html.Div(children=[html.H2(children="Prediction:"),
                               html.H2(id="prediction_result")],
                     className='prediction')],
            className='column'),
        # second column : fig performances categorical
        html.Div(children=[dcc.Graph(figure=fig_perfs, className='graph')] + cat_children,
                 className='column')
    ],
        className='row')
]
)


# The callback function will provide one "Output" in the form of a string (=children)
@app.callback(Output(component_id="prediction_result", component_property="children"),
              # The values corresponding to sliders and dropdowns of respectively numerical and categorical features
              [Input('{}-dropdown'.format(var), 'value') for var in uc.schema.name])
# The input variable are set in the same order as the callback Inputs
def update_prediction(*X):
    # get the data input and map it to the correponding feature names
    payload = dict(zip(uc.schema.name, X))
    # use predict_single
    prediction = best.predict_single(**payload)[0]
    # And retuned to the Output of the callback function
    return " {}% ".format("%.2f" % (prediction * 100))


app = app.server
