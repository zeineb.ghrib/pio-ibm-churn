import os
import previsionio as pio
import pandas as pd
import requests
from sklearn.model_selection import train_test_split

from previsionio.utils import PrevisionException
import time

from dotenv import load_dotenv, find_dotenv
# a best practice found here:
# https://drivendata.github.io/cookiecutter-data-science/
# find .env automatically by walking up directories until it's found
dotenv_path = find_dotenv()

# load up the entries as environment variables
load_dotenv(dotenv_path)
URL = os.environ.get("URL")
TOKEN = os.environ.get("TOKEN")
# some constants:
UC_NAME = 'IBM-Churn'
SEED = 42

HOME_PATH = os.getcwd()
#HOME_PATH = os.environ.get("PYTHONPATH")
DATA_PATH = os.path.join(HOME_PATH, 'data')
URL_DATA = "https://raw.githubusercontent.com/Athena75/IBM-Customer-Value-Dashboarding/main/data/Customer-Value-Analysis.csv"

pio.client.init_client(URL, TOKEN)


def get_data(url_data=URL_DATA, target_folder=DATA_PATH):
    """
    Downloads the raw dataset from the given url to the target local folder
    If the dataset already exists it returns the related local path
    :param target_folder: folder in which will be created the created csv file
    :param url_data: url hosting raw data
    :return: the downloaded csv file
    """
    target_file = os.path.join(target_folder, url_data.split('/')[-1])
    if os.path.isfile(target_file):
        with open(target_file, 'w') as f:
            # get variables names
            print('Downloading csv file')
            content = requests.get(url_data).content.decode("utf-8")
            f.write(content)
        print(f"data Downloaded successfully in {target_file}")
    return target_file


def wait_until_deployment(dataset, timeout=None, interval_time=5):
    """
    internal function allowing to wait until a newly created prevision dataset
    is completely deployed within the platform
    :param dataset: the created pio.Dataset
    :param timeout: if reached raise an exception
    :param interval_time: time interval for status checking
    """
    t0 = time.time()
    while True:
        # capture the current state
        status = dataset._status

        if (timeout is not None) and (time.time() - t0 > timeout):
            raise PrevisionException('timeout while waiting on {} deployment'
                                     .format(dataset._id))
        if status['drift'] == 'done':
            break
        time.sleep(interval_time)


def make_pio_train_test(input):
    """
    Creates a training and a holdout dataset from the original data
    :param input: path to dataset input
    :return: a tuple of the related train, test pio.Dataset
    """
    df = pd.read_csv(input)
    # make a train/test split
    train, test = train_test_split(df, test_size=0.2, random_state=SEED)
    # register the training dataset
    train_fe = pio.Dataset.new('ibm_churn_train', dataframe=train)
    # wait until it is deployed in the platform
    wait_until_deployment(train_fe, timeout=120)
    print(f"{train_fe.name} loaded with success")

    test_fe = pio.Dataset.new('ibm_churn_test', dataframe=test)
    wait_until_deployment(test_fe, timeout=120)
    print(f"{test_fe.name} loaded with success")

    return train_fe, test_fe


def create_use_case(train_fe, test_fe, target_column='Response', id_column='Customer'):
    ## auto ml use case starting
    # configure columns of datasets
    col_config = pio.ColumnConfig(target_column=target_column, id_column=id_column)

    # config use case profile
    uc_config = pio.TrainingConfig(models=[pio.Model.RandomForest, pio.Model.LinReg],
                                   simple_models=[],
                                   features=pio.Feature.Full.drop(pio.Feature.PCA,
                                                                  pio.Feature.KMeans,
                                                                  pio.Feature.PolynomialFeatures),
                                   profile=pio.Profile.Quick)

    print("launch Classification auto ml use case")
    uc = pio.Classification.fit(UC_NAME,
                                dataset=train_fe,
                                metric=pio.metrics.Classification.AUC,
                                holdout_dataset=test_fe,
                                column_config=col_config,
                                training_config=uc_config)
    # wait until we get at least 3 models
    uc.wait_until(lambda usecase: len(usecase) > 2)
    print("At least 3 models have been trained")
    return uc


# #We need to explicitly specify the host in dashboard.py to access the dashboard app from outside the container
if __name__ == "__main__":
    # Step 0 : get_data
    data_file = get_data()
    # Step 1 : make train/test datasets
    train_fe, test_fe = make_pio_train_test(data_file)
    # Step 2: create a new use case
    uc = create_use_case(train_fe, test_fe)
